This is a 2D maze game being built using Unity
We are Team Coding Ninjas 
Our team members are Dipraa Arora, Rohini, Urnisha Banerjee, Vedasree


Our basic idea of the game building is we are going to create a single player and 1 vs 1 player game.

In single player game the character has to traverse through the maze avoiding obstacles and reach the end point starting from a point.The time elapsed will be shown.


In 1 vs 1 player game we will be placing a target somwhere in the maze and the players have to reach the target avoiding the obstacles.One who reaches the target first will be the winner and will be displayed on the screen.
