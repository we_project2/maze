using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using System.Collections;

public class PlayerScript : MonoBehaviourPun
{
    public float stepSize = 1.0f;
    private Rigidbody2D rb;

    public Vector3 Player1startingpoint = new Vector3(-9, -9, -10);
    public Vector3 Player2startingpoint = new Vector3(9, 9, -10);

    PhotonView view;
    private bool isWinner = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Kinematic; // Set Rigidbody to Kinematic to prevent gravity effects
        PlaceObjectAtStart();
        view = GetComponent<PhotonView>();
        PhotonNetwork.AutomaticallySyncScene = false;
    }

    void PlaceObjectAtStart()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            transform.position = Player1startingpoint;
        }
        else
        {
            transform.position = Player2startingpoint;
        }
    }

    void Update()
    {
        if (view.IsMine)
        {
            Vector3 direction = Vector3.zero;

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                direction = Vector3.up; // Move up along the y-axis
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                direction = Vector3.down; // Move down along the y-axis
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = Vector3.left; // Move left along the x-axis
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = Vector3.right; // Move right along the x-axis
            }

            if (direction != Vector3.zero)
            {
                Move(direction);
            }
        }
    }

    void Move(Vector3 direction)
    {
        Vector3 targetPosition = transform.position + direction * stepSize;

        if (!IsPositionValid(targetPosition))
        {
            Debug.Log("Cannot move in that direction. Wall detected or out of bounds.");
            return;
        }
        
        rb.MovePosition(targetPosition);
        Debug.Log("Moved to: " + targetPosition);
    }

    bool IsPositionValid(Vector3 targetPosition)
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(targetPosition, new Vector2(0.9f, 0.9f), 0);

        foreach (Collider2D collider in colliders)
        {
            if (collider.CompareTag("Wall"))
            {
                
                return false;
            }
            else if (collider.CompareTag("Target"))
            {
                Debug.Log("Reached the target!");
                EndGame();
                return false;
            }
        }
        return true;
    }

    void EndGame()
    {
        if (view.IsMine)
        {
            isWinner = true;
        }
        photonView.RPC("OnGameEnd", RpcTarget.All, view.Owner.ActorNumber);
    }

    [PunRPC]
    void OnGameEnd(int winnerActorNumber)
    {
        //if (PhotonNetwork.LocalPlayer.ActorNumber == winnerActorNumber)
        //{
       //     isWinner = true;
       // }

        // Delay loading the scene to ensure all clients have processed the RPC
        StartCoroutine(LoadEndScene());
    }

    IEnumerator LoadEndScene()
    {
        yield return new WaitForSeconds(1.0f); // Optional delay for synchronization

        if (isWinner)
        {
            Debug.LogError("Winning scene loading ...");
            SceneManager.LoadScene("WinningScene");
        }
        else
        {
            Debug.LogError("Losing scene loading ...");
            SceneManager.LoadSceneAsync("LosingScene");
        }
    }
}
