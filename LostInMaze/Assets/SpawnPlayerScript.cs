using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnPlayerScript : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    private SpriteRenderer spriteRenderer;
    Vector3 PlayerStartingPoint = new Vector3(-9,-9,-10);
    int a = 0;
    void Start()
    {   
        spriteRenderer = GetComponent<SpriteRenderer>();
        PhotonNetwork.Instantiate(player.name,PlayerStartingPoint,Quaternion.identity);
       
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
