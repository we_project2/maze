using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player;  // Reference to the player's transform
    public Vector3 offset;    // Offset between the player and the camera

    void Start()
    {
        // Set initial offset if not set in the inspector
        if (offset == Vector3.zero)
        {
            offset = transform.position - player.position;
        }
    }

    void LateUpdate()
    {
        // Move the camera to the player's position with the offset
        transform.position = player.position + offset;
    }
}
