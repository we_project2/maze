using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using MyGame.Maze;
using Hashtable = ExitGames.Client.Photon.Hashtable;
//using System.SceneManager;
using UnityEngine.SceneManagement;
using System.Runtime.CompilerServices;


public class ConnectAndJoinRooms : MonoBehaviourPunCallbacks
{
    [SerializeField] public InputField createInput;
    [SerializeField] public InputField joinInput;

    public void CreateRoom()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            PhotonNetwork.CreateRoom(createInput.text);
        }
        else
        {
            Debug.LogError("PhotonNetwork is not connected and ready yet.");
        }
    }

    public void JoinRoom()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            PhotonNetwork.JoinRoom(joinInput.text);
            Debug.Log("Joined room");
        }
        else
        {
            Debug.LogError("PhotonNetwork is not connected and ready yet.");
        }
    }

    public override void OnJoinedRoom()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        if(PhotonNetwork.IsMasterClient)
        {
            
            PhotonNetwork.LoadLevel("2_player_maze_working");
            
        }
        
        
        
    }
}
  

