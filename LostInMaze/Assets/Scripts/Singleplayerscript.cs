using UnityEngine;
using UnityEngine.SceneManagement;

public class playerscript : MonoBehaviour
{
    public float stepSize = 1.0f;
    public float speed = 6.0f;
    private Rigidbody2D rb;
    public Vector3 mazeStartingPoint;
    public AudioSource CollisionSound;
    public AudioSource MovingSound;
    void Start()
    {
        PlaceObjectAtStart();
        rb = GetComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Kinematic; 
    }

    void PlaceObjectAtStart()
    {
        transform.position = mazeStartingPoint;
    }

    void Update()
    {
        Vector3 direction = Vector3.zero;

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            direction = Vector3.up; 
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            direction = Vector3.down; 
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            direction = Vector3.left; 
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            direction = Vector3.right;
        }

        if (direction != Vector3.zero)
        {   
            MovingSound.Play();
            Move(direction);
        }
    }

    void Move(Vector3 direction)
    {
        
        Vector3 targetPosition = transform.position + direction * stepSize;

        
        if (!IsPositionValid(targetPosition))
        {
            Debug.Log("Cannot move in that direction. Wall detected or out of bounds.");
            CollisionSound.Play();
            return;
        }

      
        rb.MovePosition(targetPosition);
        Debug.Log("Moved to: " + targetPosition);
    }

    bool IsPositionValid(Vector3 targetPosition)
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(targetPosition, new Vector2(0.9f, 0.9f), 0);

        foreach (Collider2D collider in colliders)
        {
            if (collider.CompareTag("Wall"))
            {
                return false;
            }
            else if (collider.CompareTag("Target"))
            {
                Debug.Log("Reached the target!");
                EndGame(); 
                return false; 
            }
        }

       
        return true;
    }

    void EndGame()
    {
        Debug.Log("Congratulations! You reached the target.");
        SceneManager.LoadSceneAsync(6);
    }
}
