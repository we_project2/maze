using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SoundManager : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody2D rb;
    public float stepSize = 1.0f;
    PhotonView view;
    public AudioSource CollisionSound;
    public AudioSource MovingSound;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Kinematic; 
        view = GetComponent<PhotonView>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine)
        {
            Vector3 direction = Vector3.zero;

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                direction = Vector3.up; // Move up along the y-axis
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                direction = Vector3.down; // Move down along the y-axis
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = Vector3.left; // Move left along the x-axis
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = Vector3.right; // Move right along the x-axis
            }

            if (direction != Vector3.zero)
            {
                PlaySound(direction);
            }
        }
    }
    
    void PlaySound(Vector3 direction)
    {
        Vector3 targetPosition = transform.position + direction * stepSize;

        if (!IsPositionValid(targetPosition))
        {   
            CollisionSound.Play(); 
            return;
        }
        MovingSound.Play();

    }

    bool IsPositionValid(Vector3 targetPosition)
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(targetPosition, new Vector2(0.9f, 0.9f), 0);

        foreach (Collider2D collider in colliders)
        {
            if (collider.CompareTag("Wall") || collider.CompareTag("Target") )
            {  
                return false;
            }   
        }
        return true;
    }
}
