using UnityEngine;
using Photon.Pun;

public class PlayerAppearance : MonoBehaviourPun
{
    public Color[] playerColors; // Assigned in Unity Editor
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        if (photonView.IsMine)
        {
            int playerIndex = PhotonNetwork.LocalPlayer.ActorNumber % playerColors.Length;
            spriteRenderer.color = playerColors[playerIndex];
        }
    }
}