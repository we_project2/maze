using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyGame.Maze;
using Photon.Pun;

 public class MazeManager : MonoBehaviourPunCallbacks
    {
        public MazeGenerator mazeGenerator;
    void Start()
    {
         if (PhotonNetwork.IsMasterClient)
        {
        // Generate maze data
           int[,] mazeData = mazeGenerator.GetMazeData();
           List<int> mazeDataList = ConvertMazeToList(mazeData);

        // Send maze data to all clients
            PhotonView photonView = PhotonView.Get(this);
            photonView.RPC("RPC_SetMazeData", RpcTarget.All, mazeDataList, mazeGenerator.width, mazeGenerator.height);
    }
}


        [PunRPC]
        void RPC_SetMazeData(int[,] mazeData)
        {
            mazeGenerator.SetMazeData(mazeData);
        }
    }
