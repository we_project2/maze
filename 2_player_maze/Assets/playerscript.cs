using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using System.Collections;


public class PlayerScript : MonoBehaviourPun
{
    public float stepSize = 1.0f;
    public float speed = 6.0f;
    private Rigidbody2D rb;

    public Vector3 Player1startingpoint = new Vector3(-9, -9, -10);
    public Vector3 Player2startingpoint = new Vector3(9, 9, -10);

    PhotonView view;
    private bool hasHitTarget = false;
    private bool isWinner = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.bodyType = RigidbodyType2D.Kinematic; // Set Rigidbody to Kinematic to prevent gravity effects
        PlaceObjectAtStart();
        view = GetComponent<PhotonView>();
    }

    void PlaceObjectAtStart()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            transform.position = Player1startingpoint;
        }
        else
        {
            transform.position = Player2startingpoint;
        }
    }

    void Update()
    {
        if (view.IsMine)
        {
            Vector3 direction = Vector3.zero;

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                direction = Vector3.up; // Move up along the y-axis
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                direction = Vector3.down; // Move down along the y-axis
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                direction = Vector3.left; // Move left along the x-axis
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                direction = Vector3.right; // Move right along the x-axis
            }

            if (direction != Vector3.zero)
            {
                Move(direction);
            }
        }

        if (hasHitTarget)
        {
            // Wait to make sure that OnGameEnd has finished execution
            // Use Coroutine to avoid immediate loading that might cause issues
            StartCoroutine(LoadSceneBasedOnResult());
        }
    }

    void Move(Vector3 direction)
    {
        Vector3 targetPosition = transform.position + direction * stepSize;

        if (!IsPositionValid(targetPosition))
        {
            Debug.Log("Cannot move in that direction. Wall detected or out of bounds.");
            return;
        }

        rb.MovePosition(targetPosition);
        Debug.Log("Moved to: " + targetPosition);
    }

    bool IsPositionValid(Vector3 targetPosition)
    {
        Collider2D[] colliders = Physics2D.OverlapBoxAll(targetPosition, new Vector2(0.9f, 0.9f), 0);

        foreach (Collider2D collider in colliders)
        {
            if (collider.CompareTag("Wall"))
            {
                return false;
            }
            else if (collider.CompareTag("Target"))
            {
                Debug.Log("Reached the target!");
                EndGame();
                return false;
            }
        }
        return true;
    }

    void EndGame()
    {
        if (view.IsMine) // Only the player who hit the target executes this
        {
            hasHitTarget = true;
            isWinner = true;
            photonView.RPC("OnGameEnd", RpcTarget.All, true); // Notify all players that this player won
        }
        else
        {
            photonView.RPC("OnGameEnd", RpcTarget.All, false); // Notify all players that this player lost
        }
    }

    [PunRPC]
    void OnGameEnd(bool playerWon)
    {
        if (view.IsMine)
        {
            isWinner = playerWon;
            hasHitTarget = true; // Ensure this player’s state is updated
        }
    }

    private IEnumerator LoadSceneBasedOnResult()
    {
        yield return new WaitForSeconds(1); // Optional: Adjust the delay as needed
        Debug.LogError(hasHitTarget);
        if (isWinner)
        {
            SceneManager.LoadSceneAsync("WinningScene");
        }
        else
        {
            Debug.LogError(hasHitTarget);
            SceneManager.LoadSceneAsync("LosingScene");
        }
    }
}
