using UnityEngine;
using UnityEngine.UI;

public class GameStatusUI : MonoBehaviour
{
    public Text player1StatusText;
    public Text player2StatusText;

    private void Start()
    {
        UpdatePlayerStatus(1, "Waiting...");
        UpdatePlayerStatus(2, "Waiting...");
    }

    public void UpdatePlayerStatus(int playerNumber, string status)
    {
        if (playerNumber == 1)
        {
            player1StatusText.text = "Player 1: " + status;
        }
        else if (playerNumber == 2)
        {
            player2StatusText.text = "Player 2: " + status;
        }
    }
}
