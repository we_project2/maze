using UnityEngine;
using System.Collections.Generic;
using Photon.Pun;
using ExitGames.Client.Photon;

namespace MyGame.Maze
{
    public static class ListExtensions
    {
        private static System.Random rng = new System.Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }


    public class MazeGenerator : MonoBehaviourPun
    {
        private const int V = -20;
        public int width = 21;
        public int height = 21;
        public GameObject wallPrefab;
        public GameObject floorPrefab;
        public GameObject targetPrefab;
        public float cellSize = 2.0f;

        public int[,] maze;
        private Vector3 mazeOrigin = new Vector3(-10, -10, 0);
        private Vector2Int playerStart1 = new Vector2Int(1, 1);
        private Vector2Int playerStart2;
        private List<Vector2Int> floorCells = new List<Vector2Int>();
        
        void Start()
        {
            width = EnsureOdd(width);
            height = EnsureOdd(height);
            maze = new int[width, height];

            if (PhotonNetwork.IsMasterClient)
            {
                // Generate maze on master client
                
                InitializeMaze();
            
                playerStart2 = new Vector2Int(width - 2, height - 2);

                GenerateMaze(playerStart1);
                GenerateMaze(playerStart2);

                PlaceTarget();
                maze = GetMazeData();

                // Convert maze to Array for Photon
                int[] mazeDataArray = ConvertMazeToArray(maze);

                // Broadcast maze data to all clients
                PhotonView photonView = PhotonView.Get(this);
                Debug.Log("Sending RPC_SetMazeData");
                photonView.RPC("RPC_SetMazeData", RpcTarget.AllBuffered, mazeDataArray, width, height); 
            }
            else
            {    
                Debug.LogError("I am not master client");
            }
        }

        int EnsureOdd(int value) => (value % 2 == 0) ? value + 1 : value;


       public void InitializeMaze()
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    maze[x, y] = 0;
                }
            }
        }

        void GenerateMaze(Vector2Int start)
        {
            maze[start.x, start.y] = 1;
            floorCells.Add(start);
            List<int> directions = new List<int> { 0, 1, 2, 3 };
            directions.Shuffle();

            foreach (int direction in directions)
            {
                Vector2Int next = start + DirectionToVector2Int(direction) * 2;

                if (IsInBounds(next) && maze[next.x, next.y] == 0)
                {
                    maze[next.x, next.y] = 1;
                    floorCells.Add(next);
                    Vector2Int between = start + DirectionToVector2Int(direction);
                    maze[between.x, between.y] = 1;
                    floorCells.Add(between);
                    GenerateMaze(next);
                }
            }
        }

        Vector2Int DirectionToVector2Int(int direction)
        {
            switch (direction)
            {
                case 0: return Vector2Int.down;
                case 1: return Vector2Int.right;
                case 2: return Vector2Int.up;
                case 3: return Vector2Int.left;
                default: return Vector2Int.zero;
            }
        }

        bool IsInBounds(Vector2Int position)
        {
            return position.x > 0 && position.x < width - 1 && position.y > 0 && position.y < height - 1;
        }

        void PlaceTarget()
        {
            Vector2Int targetPosition;
            do
            {
                targetPosition = floorCells[Random.Range(0, floorCells.Count)];
            } while (maze[targetPosition.x, targetPosition.y] != 1);

            maze[targetPosition.x, targetPosition.y] = 2;
        }

        void DrawMaze()
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Vector3 position = new Vector3(x * cellSize, y * cellSize, 0f) + mazeOrigin;
                    GameObject prefab = GetPrefabForCell(maze[x, y]);
                    if (prefab != null)
                    {
                        Instantiate(prefab, position, Quaternion.identity);
                    }
                }
            }
        }

        GameObject GetPrefabForCell(int cellType)
        {
            switch (cellType)
            {
                case 0: return wallPrefab;
                case 1: return floorPrefab;
                case 2: return targetPrefab;
                default: return null;
            }
        }

        // Method to get the maze data
        public int[,] GetMazeData()
        {
            return maze;
        }

        public int[] ConvertMazeToArray(int[,] maze)
        {
            int[] mazeArray = new int[maze.GetLength(0) * maze.GetLength(1)];
            for (int x = 0; x < maze.GetLength(0); x++)
            {
                for (int y = 0; y < maze.GetLength(1); y++)
                {
                    mazeArray[x * maze.GetLength(1) + y] = maze[x, y];
                }
            }
            return mazeArray;
        }

        public int[,] ConvertArrayToMaze(int[] mazeArray, int width, int height)
        {
            int[,] maze = new int[width, height];
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    maze[x, y] = mazeArray[x * height + y];
                }
            }
            return maze;
        }


        [PunRPC]
        
        public void RPC_SetMazeData(int[] mazeDataArray, int width, int height)
        {
            maze = ConvertArrayToMaze(mazeDataArray, width, height);
            DrawMaze();
        }
    }
}

 



  




