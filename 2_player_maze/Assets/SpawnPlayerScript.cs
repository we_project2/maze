using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnPlayerScript : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    Vector3 PlayerStartingPoint = new Vector3(-9,-9,-10);
    void Start()
    {
        PhotonNetwork.Instantiate(player.name,PlayerStartingPoint,Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
