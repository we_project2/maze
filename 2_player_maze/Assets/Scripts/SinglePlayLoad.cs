using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SinglePlayLoad : MonoBehaviour
{
    public void MultiPlay()
    {
        SceneManager.LoadSceneAsync(3);
    }
    public void SinglePlay()
    {
        SceneManager.LoadSceneAsync(2);
    }
    public void QuitGame()
    {
        Application.Quit();
    } 
}
