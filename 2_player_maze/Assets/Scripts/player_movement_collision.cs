using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_movement_collision : MonoBehaviour
{
    public GameObject player;
    public GameObject wallPrefab;

    private int dim = 50;
    private int Life_left = 3;
    private int score = 0;
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            MovePlayer(0, dim);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            MovePlayer(0, -dim);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            MovePlayer(dim, 0);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            MovePlayer(-dim, 0);
        }

        float remainingTime = 100000 - (Time.time - startTime);
        Debug.Log("Time: " + (remainingTime / 1000));
    }

    private void MovePlayer(int deltaX, int deltaY)
    {
        Vector3 new_position = player.transform.position + new Vector3(deltaX, deltaY, 0);

        // Perform collision detection
        if (new_position.x >= 0 && new_position.x <= dim && new_position.y >= 0 && new_position.y <= dim)
        {
            // Create a temporary wall object for collision detection
            GameObject temp_wall = Instantiate(wallPrefab, new_position, Quaternion.identity);

            // Check collisions with all_elements
            Collider[] colliders = Physics.OverlapBox(new_position, Vector3.one * dim / 2);
            foreach (var collider in colliders)
            {
                if (collider.gameObject != player)
                {
                    // Handle collision with elements
                    if (collider.CompareTag("WrongWall"))
                    {
                        Life_left--;
                        Destroy(temp_wall);
                        player.transform.position = Vector3.zero; // Reset player position upon collision
                    }
                    else if (collider.CompareTag("Gold"))
                    {
                        score += 10;
                        Destroy(collider.gameObject);
                    }
                }
            }
        }
    }
}
