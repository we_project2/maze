using UnityEngine;
using System.Collections.Generic;

public class MazeGenerator : MonoBehaviour
{
    public int width = 11; 
    public int height = 11; 
    public GameObject wallPrefab;
    public GameObject floorPrefab;
    public GameObject targetPrefab;

    private int[,] maze;
    private System.Random rand = new System.Random();

    void Start()
    {
       
        if (width % 2 == 0) width++;
        if (height % 2 == 0) height++;

        maze = new int[width, height];
        GenerateMaze();
        DrawMaze();
        PlaceTarget();
    }

    void GenerateMaze()
    {
        
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                maze[x, y] = 1;
            }
        }

     
        CarvePassagesFrom(1, 1);
    }

    void CarvePassagesFrom(int cx, int cy)
    {
    
        int[] dx = { 0, 0, 2, -2 };
        int[] dy = { -2, 2, 0, 0 };
        int[] directionOrder = { 0, 1, 2, 3 };
        Shuffle(directionOrder);

        for (int i = 0; i < directionOrder.Length; i++)
        {
            int nx = cx + dx[directionOrder[i]];
            int ny = cy + dy[directionOrder[i]];

            if (nx > 0 && nx < width && ny > 0 && ny < height && maze[nx, ny] == 1)
            {
                maze[nx, ny] = 0; 
                maze[cx + dx[directionOrder[i]] / 2, cy + dy[directionOrder[i]] / 2] = 0; 
                CarvePassagesFrom(nx, ny);
            }
        }
    }

    void Shuffle(int[] array)
    {
        for (int i = array.Length - 1; i > 0; i--)
        {
            int randomIndex = rand.Next(i + 1);
            int temp = array[i];
            array[i] = array[randomIndex];
            array[randomIndex] = temp;
        }
    }

    void DrawMaze()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                GameObject prefab = maze[x, y] == 1 ? wallPrefab : floorPrefab;
                Instantiate(prefab, new Vector2(x, y), Quaternion.identity);
            }
        }
    }

    

    void PlaceTarget()
    {
        Vector2 targetPosition = new Vector2(width - 2, height - 2);
        Instantiate(targetPrefab, targetPosition, Quaternion.identity);
    }
}
