using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ButtonsAction : MonoBehaviour
{
    public void PlayAgain()
    {
        SceneManager.LoadSceneAsync(1);
    }

    public void QuitGame(){
        SceneManager.LoadSceneAsync(0);
    }
}
